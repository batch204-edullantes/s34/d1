// require directive is use to load the express module/packages
// It allows us to access the methods and function in easily creating our server.

const express = require("express");
// This creates an express application and stores this in a constant called app.

// In layman's term, app is our server.

const app = express();

// Setup for allowing the server to handle data from requests
app.use(express.json()); // Allow your app to read json data

// Express has methods corresponding to each HTTP method.
// "/" corresponds with our base URI
// localhost:3000/
app.get("/", (req, res) => {
	// res.send uses the express JS module's method to send a response back to the client.
	res.send("Hello World");
});

// This route expects to received a POST request at the URI /endpoint "/hello"

app.post("/hello", (req, res) => {
	// req.body contains the contents/data of the request
	console.log(req.body);
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`);
})

/*
	SCENARIO:
		We want to create a simple users database that will perform CRUD operations based on the client request. The following routes should perform its functionality:
*/

	// mock database for our users
	let users = [
		{
			username: "janedoe",
			password: "jane123"
		},
		{
			username: "johnsmith",
			password: "john123"
		}
	];

	// This route expects to receive a POST request at the URI "/singup"
	// This will create a user object in the "users" array that mirrors a real world registration:
		// All fields are required/fill out.

	app.post("/signup", (req, res) => {
		console.log(req.body);

		// If contents of "req.body" with property of "username" and "password" is not empty.
		if(req.body.username !== "" && req.body.password !== "" && req.body.username !== undefined && req.body.password !== undefined) {
			// To store the req.body sent via Postman in the users array we will use "push" method.
			// user object will be saved in our users array.
			users.push(req.body);

			res.send(`User ${req.body.username} successfully registered!`)
		} 
		// If the username and password are not complete.
		else {
			res.send("Please input both username and password")
		}
	});

	// This route expects to receive a GET request at the URI "/users"
	app.get("/users", (req, res) => {
		res.send(users);
	});

	// This route expects to receive a PUT request at the URI "/change-password"
	// This will update the password of a user that matches the information provided in the client/Postman
		// We will use the username as the search property

	app.put("/change-password", (req, res) => {
		// Create a variable to store the message to be sent back to the client/Postman (response)
		let message;

		// Create a variable to store the message to be sent back to the client/Postman (response)

		for(let i = 0; i < users.length; i++) {
			// if the "username" provided in the client/Postman and username of the current element/object in the loop is the same.
			if(users[i].username === req.body.username) {
				// Changes the passowrd of the user found by the loop into the password provided in the client/Postman.
				users[i].password = req.body.password

				message = `User ${req.body.username}'s password has been updated.`

				// Break out of the loop once the user that matches the username provided in the client/Postman found
				break;
			} 
			// If no user was found
			else {
				// Changes the message to be sent back as the response.
				message = "User does not exist!";
			}
		}

		res.send(message);
	});

	// This route expects to receive a DELETE request at the URI "/delete/user"
	// This will remove the user from the array for deletion

	app.delete("/delete-user", (req, res) => {
		// Create a variable to store the message to be sent back to the client/Postman (response)
		let message;

		// Create a variable to store the message to be sent back to the client/Postman (response)

		for(let i = 0; i < users.length; i++) {
			// if the "username" provided in the client/Postman and username of the current element/object in the loop is the same.
			if(users[i].username === req.body.username) {
				// The splice method manipulates the array and removes the user object from the "users" array based on it's index.
				users.splice(users[i], 1);

				message = `User ${req.body.username}'s password has been deleted.`

				// Break out of the loop once the user that matches the username provided in the client/Postman found
				break;
			} 
			// If no user was found
			else {
				// Changes the message to be sent back as the response.
				message = "User does not exist!";
			}
		}

		res.send(message);
	});

// For our application server to run, we need a port to listen to

const port = 3000;

// Returns a message to confirm that the server is running in the terminal.
app.listen(port, () => console.log(`Server is running at ${port}`));